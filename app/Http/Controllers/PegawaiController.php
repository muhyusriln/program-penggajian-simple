<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
    public function index() {
        // mengambil data dari table pegawai
        $pegawai = DB::table('tblpegawai')->paginate(10);

        // mengirim data pegawai ke view index
        return view('index', ['pegawai' => $pegawai]);
    }

    public function cari(Request $request) {
        // menangkap data pencarian
        $cari = $request->cari;

        // mengambil data dari table pegawai sesuai pencarian data
        $pegawai = DB::table('tblpegawai')
        ->where('nama', 'like',"%".$cari."%")
        ->paginate();

        // mengirim data pegawai ke view index
        return view('index', ['pegawai' => $pegawai]);
    }

    // method untuk menampilkan view form tambah pegawai
    public function tambah() {
        // memanggil view tambah
        return view('tambah');
    }

    // method untuk insert data ke table pegawai
    public function store(Request $request) {
        // insert data ke table pegawai
        DB::table('tblpegawai')->insert([
            'nama' => $request->nama,
            'jabatan' => $request->jabatan,
            'umur' => $request->umur,
            'alamat' => $request->alamat
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/pegawai');
    }

    // method untuk edit data pegawai
    public function edit($id) {
        // mengambil data pegawai berdasarkan id yang dipilih
        $pegawai = DB::table('tblpegawai')->where('id',$id)->get();
        // passing data pegawai yang didapat ke view edit.blade.php
        return view('edit', ['pegawai' => $pegawai]);
    }

    // method untuk update data pegawai
    public function update(Request $request) {
        // update data pegawai
        DB::table('tblpegawai')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'jabatan' => $request->jabatan,
            'umur' => $request->umur,
            'alamat' => $request->alamat
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/pegawai');
    }

    // method untuk hapus data pegawai
    public function hapus($id) {
        // menghapus data pegawai berdasarkan id yang dipilih
        DB::table('tblpegawai')->where('id', $id)->delete();
        // alihkan halaman ke halaman pegawai
        return redirect('/pegawai');
    }
    
}
