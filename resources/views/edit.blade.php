<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Tutorial Membuat CRUD Pada Laravel</title>
</head>
<body>
  <h2><a>www.startdev.co.id</a></h2>
  <h3>Data Pegawai</h3>

  <a href="/pegawai">Kembali</a> <br/><br/>

  @foreach($pegawai as $p)
  <form action="/pegawai/update" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{$p->id}}"> <br/>
    <label name="nama">Nama</label>
    <input type="text" required="required" name="nama" value="{{$p->nama}}"> <br/>
    <label name="jabatan">Jabatan</label>
    <input type="text" required="required" name="jabatan" value="{{$p->jabatan}}"> <br/>
    <label name="umur">Umur</label>
    <input type="number" required="required" name="umur" value="{{$p->umur}}"> <br/>
    <label name="alamat">Alamat</label>
    <textarea required="required" name="alamat">{{$p->alamat}}</textarea> <br/>
    <input type="submit" value="Simpan Data">
  </form>
  @endforeach
    
</body>
</html>