<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Tutorial Membuat CRUD Pada Laravel</title>
</head>
<body>
  <h2><a>www.startdev.co.id</a></h2>
  <h3>Data Pegawai</h3>

  <a href="/pegawai">Kembali</a> <br/><br/>  

  <form action="/pegawai/store" method="POST">
    {{ csrf_field() }}
    <label name="nama">Nama</label>
    <input type="text" name="nama" required="required"> <br/>
    <label name="jabatan">Jabatan</label>
    <input type="text" name="jabatan" required="required"> <br/>
    <label name="umur">Umur</label>
    <input type="number" name="umur" required="required"> <br/>
    <label name="alamat">Alamat</label>
    <textarea name="alamat" required="required"></textarea> <br/>
    <input type="submit" value="Simpan Data">
  </form>
</body>
</html>